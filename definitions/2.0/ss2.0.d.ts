type RECORD_NAMES =
    "transaction" |
    "item" |
    "transaction" |
    "inventorynumber" |
    "customrecordxpel_roll" |
    "employee" |
    "customer" |
    any;

declare interface XPEL3rdPartyLibraries_Utils {
    /**
     *
     * @param label {string}
     * @param data {{}[]}
     * @param form {form}
     * @param sublist_type {string}
     * @param sublist_id {string}
     * @param [dyn_cols={}] {{[field: string]: Function}}
     * @param [buttons=[]] {Object[]}
     * @param [remove_key_txt=null] {string}
     * @param [only_show_keys=[]] {string[]}
     */
    renderSublistFromJsonData: (label, data, form, sublist_type, sublist_id, dyn_cols, buttons, remove_key_txt, only_show_keys) => void;
}

declare interface query {
    Type: {
        TRANSACTION: string,
    };
    create: (args: { type: string }) => {
        autoJoin: (args: { fieldId: string }) => void,
        columns: any[],
        sort: any[],
        runPaged: (args: { pageSize: number }) => {
            pageRanges: any[],
            count: number,
        },
    };
    createSort: (args: { column: string, ascending: boolean }) => void;
}

declare interface auth {
    createSecretKey: (args: { guid: any, encoding: string }) => string;
}

declare interface xml {
    escape: (args: any) => string;
}

declare interface hmac {
    digest: (options: any) => string;
    update: (options: any) => void;
}


declare interface crypto {
    createSecretKey: (args: { guid: any, encoding: string }) => string;
    createHmac: (args: { algorithm: any, key: any }) => hmac;
}

declare interface cache {
    getCache: (args: { name: string, scope?: any }) => any;
    Scope: {
        PRIVATE: any,
    };
    get: (args: { key: any, loader: Function, ttl: number }) => any;
    put: (args: { key: any, value: any, ttl: number }) => void;
}

declare interface format {
    format: (args: { value: any, type: string }) => any;
    parse: (args: { value: any, type: string }) => any;
}

declare interface util {
    isArray: (obj: any) => boolean;
    isBoolean: (obj: any) => boolean;
    isDate: (obj: any) => boolean;
    each: (iterable: any, callback: (args?: any) => any) => any;
}

declare interface https {
    CacheDuration: string;
    Method: {
        POST: string
    };
    ClientResponse: {
        body: string,
        code: number,
        headers: { [key: string]: string },
    };
    ServerRequest: {
        body: string,
        files: { [key: string]: any },
        headers: { [key: string]: any },
        method: string,
        parameters: { [key: string]: string | number },
        url: string,
        getLineCount: (args: { group: string }) => void,
        getSublistValue: (args: { group: string, name: string, line: string }) => void,
    };
    ServerResponse: {
        addHeader: (args: { name: string, value: string }) => void;
        getHeader: (args: { name: string }) => void;
        renderPdf: (args: { xmlString: string }) => void;
        sendRedirect: (args: {
            type: string,
            identifier: string,
            id: string,
            editMode: boolean,
            parameters: { [key: string]: string | number },
        }) => void;
        setCdnCacheable: (args: any) => void;
        setHeader: (args: any) => void;
        write: (args: any) => void;
        writeFile: (args: any) => void;
        writeLine: (args: any) => void;
        writePage: (args: any) => void;
    };
    get: (args: { url: string, headers?: { [key: string]: string } }) => {
        body: string,
        code: number,
        headers: any,
    };
    delete: (args: { url: string, headers: { [key: string]: string } }) => void;
    post: (args: { url: string, body?: string | any, headers?: { [key: string]: string } }) => clientResponse;
    put: (args: { url: string, body: string | any, headers: { [key: string]: string } }) => void;
    request: (args: { method: string, url: string, body: string | any, headers: { [key: string]: string } }) => any;
}

declare interface http {
    CacheDuration: string;
    Method: string;
    ClientResponse: {
        body: string,
        code: number,
        headers: { [key: string]: string },
    };
    ServerRequest: {
        body: string,
        files: { [key: string]: any },
        headers: { [key: string]: any },
        method: string,
        parameters: { [key: string]: string | number },
        url: string,
        getLineCount: (args: { group: string }) => void,
        getSublistValue: (args: { group: string, name: string, line: string }) => void,
    };
    ServerResponse: {
        addHeader: (args: { name: string, value: string }) => void;
        getHeader: (args: { name: string }) => void;
        renderPdf: (args: { xmlString: string }) => void;
        sendRedirect: (args: {
            type: string,
            identifier: string,
            id: string,
            editMode: boolean,
            parameters: { [key: string]: string | number },
        }) => void;
        setCdnCacheable: (args: any) => void;
        setHeader: (args: any) => void;
        write: (args: any) => void;
        writeFile: (args: any) => void;
        writeLine: (args: any) => void;
        writePage: (args: any) => void;
    };
    get: (args: { url: string, headers: { [key: string]: string } }) => void;
    delete: (args: { url: string, headers: { [key: string]: string } }) => void;
    post: (args: { url: string, body: string | any, headers: { [key: string]: string } }) => clientResponse;
    put: (args: { url: string, body: string | any, headers: { [key: string]: string } }) => void;
    request: (args: { method: string, url: string, body: string | any, headers: { [key: string]: string } }) => void;
}

declare interface clientResponse {
    body: string;
    code: number;
    headers?: any;
}

declare interface log {
    debug: (arg: { title: string, details: any } | any, opts?: any) => void;
    error: (arg: { title: string, details: any } | any, opts?: any) => void;
    audit: (arg: { title: string, details: any } | any, opts?: any) => void;
}

declare class sublist {
    addField: (args: {
        id: string,
        type: string,
        label: string,
        source?: string,
    }) => field;
    setSublistValue: (args: { sublistId: string, fieldId: string, line: any, value: any }) => sublist;
    addRefreshButton: () => void;
    displayType: string;
}

declare interface encode {
    convert: (args: {
        string: string,
        inputEncoding: string,
        outputEncoding: string
    }) => string;
    Encoding: {
        UTF_8: string,
        BASE_64: string,
    }
}

declare class record {
    id: number | string;
    isDynamic: boolean;
    Type: any;
    attach: (args: {
        record: {
            type: string,
            id: number,
        },
        to: {
            type: string,
            id: number,
        },
    }) => void;
    create: (args: { type: string, isDynamic?: boolean }) => record;
    load: (args: { type: string, id: number, isDynamic?: boolean }) => record;
    save: (args?: { enableSourcing: boolean, ignoreMandatoryFields: boolean }) => number;
    delete: (args: { type: string, id: number }) => void;
    setValue: (args: { fieldId: string, value: any, ignoreFieldChange?: boolean }) => void;
    setText: (args: { fieldId: string, text: any, ignoreFieldChange?: boolean }) => void;
    getValue: (args: any) => any;
    getText: (args: { fieldId: string } | string) => string;
    getField: (args: { fieldId: string }) => any;
    getLineCount: (args: { sublistId: string }) => number;
    getSublistValue: (args: {
        sublistId: string,
        fieldId: string,
        line: number
    }) => any;
    getSublistText: (args: {
        sublistId: string,
        fieldId: string,
        line: number
    }) => string;
    getSublists: () => string[];
    getFields: () => any[];
    submitFields: (args: {
        type: string,
        id: number,
        values: { [key: string]: any },
        options?: any
    }) => any;
    copy: (args: {
        type: string,
        id: number,
        isDynamic?: boolean,
        defaultValues?: { [key: string]: any }
    }) => record;
    insertLine: (args: any) => void;
    getSublistSubrecord: (args: { fieldId: string, sublistId: string, line: number }) => any;
    getSubrecord: (args: { fieldId: string }) => any;
    getSublistField: (args: { fieldId: string, sublistId: string, line: number }) => any;
    getSublistFields: (args: { sublistId: string }) => any;
    findSublistLineWithValue: (args: { fieldId: string, sublistId: string, value: any }) => any;
    getCurrentSublistSubrecord: (args: { fieldId: string, sublistId: string }) => any;
    getCurrentSublistValue: (args: { fieldId: string, sublistId: string }) => any;
    getCurrentSublistText: (args: { fieldId: string, sublistId: string }) => string;
    commitLine: (args: { sublistId: string }) => void;
    removeLine: (args: {
        sublistId: string,
        line: number,
        ignoreRecalc?: boolean
    }) => void;
    removeSublistSubrecord: (args: { fieldId: string, sublistId: string, line: number }) => void;
    setCurrentSublistValue: (args: {
        fieldId: string,
        sublistId: string,
        value: any,
        ignoreFieldChange?: boolean,
    }) => void;
    selectLine: (args: { line: number, sublistId: string }) => void;
    selectNewLine: (args: { sublistId: string }) => void;
    removeSubrecord: (args: { fieldId: string }) => void;
    setSublistValue: (args: { fieldId: string, sublistId: string, line: number, value: any }) => void;
    hasCurrentSublistSubrecord: (args: { fieldId: string, sublistId: string }) => boolean;
    transform: (args: { fromType: string, fromId: number, toType: string, isDynamic?: boolean }) => record;
}

declare interface serverWidget {
    createForm: (args: { title: string, hideNavBar: boolean }) => form;
    createAssistant: (args: { title: string, hideNavBar: boolean }) => form;
    FieldType: {
        RADIO: string;
        TEXT: string,
        EMAIL: string,
        TEXTAREA: string,
        INLINEHTML: string,
        SELECT: string,
        FLOAT: string,
        LONGTEXT: string,
        DATE: string,
        DATETIMETZ: string,
        LABEL: string,
        CURRENCY: string,
        HIDDEN: string,
        INTEGER: string,
        URL: string,
        CHECKBOX: string,
        RICHTEXT: string,
        MULTISELECT: string,
    };
    SublistDisplayType: {
        Normal: string,
    };
    FieldLayoutType: {
        NORMAL: string,
        OUTSIDE: string,
        OUTSIDEBELOW: string,
        OUTSIDEABOVE: string,
        MIDROW: string,
        ENDROW: string,
        STARTROW: string,
    };
    FieldBreakType: {
        STARTCOL: string,
    };
    SublistType: {
        LIST: string,
        INLINEEDITOR: string,
        STATICLIST: string,
    };
    FieldDisplayType: {
        DISABLED: any;
        INLINE: string,
        HIDDEN: string,
        READONLY: string,
        NORMAL: string,
    };
    FormPageLinkType: {
        CROSSLINK: string,
    };
}

declare class form {
    addField: (args: { id: string, type: string, label: string, container?: string, source?: any }) => field;
    addSublist: (args: { id: string, type: string, label: string }) => sublist;
    addPageLink: (args: { type: string, title: string, url: string }) => void;
    addSubmitButton: (args: { label: string }) => void;
    addFieldGroup: (args: { id: string, label: string, tab?: string }) => any;
    updateDefaultValues: (args: { [key: string]: any }) => void;
    setDefaultValues: (args: { [key: string]: any }) => void;
    addTab: (args: any) => void;
    addButton: (args: any) => void;
    clientScriptFileId: any;
    clientScriptModulePath: string;
}

declare class field {
    isMandatory: boolean;
    displaySize: {
        width: number,
        height: number
    };
    layoutType: string;
    breakType: string;
    defaultValue: string | number;
    updateDisplayType: (args: { displayType: string }) => void;
    addSelectOption: (args: { value: any, text: string, isSelected?: boolean }) => void;
    updateLayoutType: (args: {
        layoutType: serverWidget["FieldLayoutType"]
    }) => void;
}

declare class scriptContext {
    sublistId: string;
    currentRecord: record;
    fieldId: string;
    line: string;
    column: string;
    mode: string;
}

declare class context {
    response: response;
    request: request;
    newRecord: record;
    oldRecord: record;
    form: form;
    type: string;
    UserEventType: {
        EDIT: string,
        CREATE: string,
        VIEW: string,
    }
}

declare interface contextSublistChanged {
    currentRecord: record;
    sublistId: string;
}

declare interface contextFieldChanged {
    currentRecord: record;
    sublistId: string;
    fieldId: string;
    line: string;
    column: string;
}

declare interface contextPageInit {
    mode: 'copy' | 'edit' | 'create';
    currentRecord: record;
}

declare interface request {
    parameters: any;
    method: string
}

declare interface response {
    writePage: (form: form) => void;
    write: (html: string) => void;
}

declare interface email {
    send: (args: {
        author: number,
        recipients: string | number | number[],
        subject: string,
        body: string
    }) => void;
}

declare class file {
    id: number;
    url: string;
    load: (args: { id: number | string } | string) => file;
    getContents: () => string;
    create: (args: {
        name: string,
        fileType: string,
        contents: string,
        folder: number,
        isOnline?: boolean,
    }) => file;
    Type: {
        WORD: string,
        PDF: string,
        HTMLDOC: string,
        XMLDOC: string,
        CSV: string,
    };
    Encoding: {
        UTF8: string,
    }
}

declare class user {
    id: number;
    location: number;
    department: number;
    email: string;
    name: string;
    role: string;
    roleCenter: string;
    roleId: string;
    subsidiary: number;
}

declare interface session {
    get: (name: string) => any;
    set: (name: string, value: any) => void;
}

declare class script {
    id: string;
    deploymentId: string;
    logLevel: string;
    percentComplete: number;
    bundleIds: number[];
}

declare interface runtime {
    accountId: number;
    envType: string;
    getCurrentSession: () => session;
    getCurrentUser: () => user;
    getCurrentScript: () => {
        id: number,
        deploymentId: number,
        getParameter: (args: { name: string }) => any
    };
}

declare interface searchResultColumn {
    name: string;
    join: null;
    summary: null;
    label: null;
    type: 'email' | 'select' | 'text' | 'checkbox' | 'integer';
    function: null;
    formula: null;
    sortdir: string;
    whenorderedby: null;
    whenorderedbyjoin: null;
}

declare class searchResult {
    id: any;
    recordType: any;
    getValue: (args: { summary?: string, name: string, join?: string } | string) => any;
    getText: (args: { name: string, join?: string } | string) => string;
    columns: searchResultColumn[];
}

declare interface createdSearch {
    run: () => {
        each: (func: (result: searchResult) => boolean | void) => void;
        getRange: (args?: any) => any;
        runPaged: (args?: any) => any;
    }

    runPaged: (params?: any) => any
}

declare interface search {
    Sort: {
        ASC: string,
        DESC: string,
    },
    Type: {
        CUSTOM_RECORD: string,
    };
    Operator: {
        ISEMPTY: string,
        ISNOTEMPTY: string,
    },
    Summary: {
        GROUP: string,
    },
    create: (args: { type: RECORD_NAMES, columns: any[], filters?: any[] }) => search;
    lookupFields: (args: { type: string, id: number, columns: string[] }) => string[];
    createFilter: (args: {
        name: string,
        operator: string,
        values?: any[],
        formula?: string,
        join?: string
    }) => void;
    createColumn: (args: {
        name: string,
        summary?: string,
        join?: string,
        sort?: string,
    }) => any;
    load: (args?: any) => any;
    global: (args: { keywords: string }) => searchResult[];
    run: () => {
        each: (func: (result: searchResult) => boolean | void) => void;
        getRange: (args?: any) => any;
        runPaged: (args?: any) => {
            count: number,
            pageRanges: any[],
            pageSize: number,
            searchDefinition: any,
            fetch: (args?: any) => any,
        };
    };

    runPaged: (args?: { pageSize: number }) => {
        count: number,
        pageRanges: any[],
        pageSize: number,
        searchDefinition: any,
        fetch: (args?: any) => {
            data: any,
            pagedData: any,
            pageRange: any,
        },
    };
}

declare class mapSummary {
    dateCreated: any;
    seconds: any;
    usage: any;
    concurrency: any;
    yields: any;
    keys: any;
    errors: any[];
}

declare class mapContext {
    isRestarted: boolean;
    key: string;
    value: any;
    write: (key: string, value: any) => void;
}

declare class reduceContext {
    isRestarted: boolean;
    key: string;
    values: any[];
    write: (key: string, value: any) => void;
}

declare class summaryContext {
    dateCreated: any;
    seconds: any;
    usage: any;
    concurrency: any;
    yields: any;
    inputSummary: any;
    mapSummary: any;
    reduceSummary: any;
    output: {
        iterator: () => {
            each: (func: (key: string, value: string) => any) => void
        },
    };
    isRestarted: boolean;
}

declare interface task {
    TaskType: {
        MAP_REDUCE: string
    };

    create(args: { taskType: string }): void;
}

declare interface url {
    HostType: {
        APPLICATION: string,
    };

    resolveRecord(args: {
        recordType: string,
        recordId: number
    }): void;

    resolveScript(args: {
        scriptId: string,
        deploymentId: string,
        returnExternalUrl: boolean,
    }): void;

    resolveDomain(args?: any): string;
}

declare class renderer {
    addCustomDataSource: (args: { format: string, alias: string, data: any }) => void;
    renderAsString: () => string;
    templateContent: string;
}

declare interface render {
    DataSource: {
        OBJECT: string,
        JSON: string,
    };
    templateContent: string;
    create: () => renderer;
}

declare class redirect {
    toSuitelet: (args: {
        scriptId: string;
        deploymentId: string;
        parameters: { [key: string]: number | string | null };
    }) => void;
}
